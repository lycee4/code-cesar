import tkinter as tk
from tkinter import messagebox

def chiffrer_message(message, cle):
    message_chiffre = ""
    for char in message:
        if char.isalpha():
            decalage = ord('a') if char.islower() else ord('A')
            char_chiffre = chr((ord(char) - decalage + cle) % 26 + decalage)
            message_chiffre += char_chiffre
        else:
            message_chiffre += char
    return message_chiffre

def ouvrir_page_dechiffrement(cle):
    fenetre_dechiffrement = tk.Toplevel(root)
    fenetre_dechiffrement.title("Page de déchiffrement")

    def dechiffrer_message():
        message_a_dechiffrer = zone_texte.get("1.0", tk.END).strip()
        message_dechiffre = chiffrer_message(message_a_dechiffrer, -cle)
        etiquette_dechiffre.config(text=f"Message décrypté :\n{message_dechiffre}")

    def copier_message():
        message_a_copier = etiquette_dechiffre.cget("text").split(":\n")[1]
        root.clipboard_clear()
        root.clipboard_append(message_a_copier)
        messagebox.showinfo("Copie réussie", "Message copié dans le presse-papiers.")

    zone_texte = tk.Text(fenetre_dechiffrement, height=5, width=30)
    zone_texte.pack(pady=10)

    bouton_dechiffrer = tk.Button(fenetre_dechiffrement, text="Décrypter", command=dechiffrer_message)
    bouton_dechiffrer.pack(pady=10)

    etiquette_dechiffre = tk.Label(fenetre_dechiffrement, text="")
    etiquette_dechiffre.pack(pady=10)

    bouton_copier = tk.Button(fenetre_dechiffrement, text="Copier", command=copier_message)
    bouton_copier.pack(pady=10)

def ouvrir_page_chiffrement(cle):
    fenetre_chiffrement = tk.Toplevel(root)
    fenetre_chiffrement.title("Page de chiffrement")

    def chiffrer_message_et_afficher():
        message_a_chiffrer = zone_texte.get("1.0", tk.END).strip()
        message_chiffre = chiffrer_message(message_a_chiffrer, cle)
        etiquette_chiffre.config(text=f"Message chiffré :\n{message_chiffre}")

    def copier_message():
        message_a_copier = etiquette_chiffre.cget("text").split(":\n")[1]
        root.clipboard_clear()
        root.clipboard_append(message_a_copier)
        messagebox.showinfo("Copie réussie", "Message copié dans le presse-papiers.")

    zone_texte = tk.Text(fenetre_chiffrement, height=5, width=30)
    zone_texte.pack(pady=10)

    bouton_chiffrer = tk.Button(fenetre_chiffrement, text="Chiffrer", command=chiffrer_message_et_afficher)
    bouton_chiffrer.pack(pady=10)

    etiquette_chiffre = tk.Label(fenetre_chiffrement, text="")
    etiquette_chiffre.pack(pady=10)

    bouton_copier = tk.Button(fenetre_chiffrement, text="Copier", command=copier_message)
    bouton_copier.pack(pady=10)

# Fonction appelée lors de la vérification de la clé
def verifier_cle():
    cle_entree = champ_cle.get()
    try:
        cle = int(cle_entree)
        if 1 <= cle <= 25:
            messagebox.showinfo("Clé valide", "Clé correcte ! Accès autorisé.")
            ouvrir_page_dechiffrement(cle)
            ouvrir_page_chiffrement(cle)
        else:
            messagebox.showwarning("Clé invalide", "La clé doit être un entier entre 1 et 25.")
    except ValueError:
        messagebox.showwarning("Clé invalide", "La clé doit être un entier.")

# Création de la fenêtre principale
root = tk.Tk()
root.title("Application de chiffrement")

# Interface pour entrer la clé
etiquette_cle = tk.Label(root, text="Entrez la clé :")
etiquette_cle.pack(pady=10)

champ_cle = tk.Entry(root, show="*")
champ_cle.pack(pady=10)

bouton_verifier_cle = tk.Button(root, text="Vérifier la clé", command=verifier_cle)
bouton_verifier_cle.pack(pady=10)

# Lancement de la boucle principale
root.mainloop()
